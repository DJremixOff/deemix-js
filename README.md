<div align="center">
	<img src="assets/logo.svg" alt="Deemix Logo" width="256" height="80">
  <p align="center">A barebone deezer downloader library from the nice work of RemixDev !</p>
  <p align="center"><a href="https://www.reddit.com/r/deemix/">Reddit</a> • <a href="https://gitlab.com/RemixDev/deemix-js">Original project</a> • <a href="https://t.me/deemixbuildbot">Telegram</a></p>
</div>

<hr>

## Get Started
If you're an __user__ you can [download deemix-gui](https://gitlab.com/RemixDev/deemix-gui/), an easy to use application that uses the deemix libs!

If you're a __developer__ documentation, you can check instalation and how to use it below.
## Installation
Install deemix with :\
`npm i deemix`\
And import it in your project with :\
`const deemix = require('deemix')`

**Some functions require** [Deezer-JS](https://gitlab.com/RemixDev/deezer-js). You can install it with :\
`npm i deezer-js`
## Usage
### • Root function
#### Parse Link (async)
```javascript
deemix.parseLink('link')
```
Return an array `['link','type','id']`
#### generateDownloadObject (async)
> **[!] Require Deezer-js**\
> [Go here](#Deezer-JS) to see how it works.

>**(i) Bitrate is an array**\
> [Go here](#Bitrate) to see how it works.

```javascript
deemix.generateDownloadObject(deezer-js, 'link', bitrate)
```
Return Download Object

### • Downloader function
#### Create new Downloader
> **[!] Require Deezer-js**\
> [Go here](#Deezer-JS) to see how it works.

For download a track, you have to create a new Downloader :
```javascript
let downloader = new Deezer.downloader.Downloader(deezer-js, downloadObject)
```
#### downloader.start (async)
```javascript
await downloader.start()
```
Return **nothing**

To determine whether the downloader has encountered an error or if the download has been successful, you can check it with the Downloader Object
```javascript
if (downloader.downloadObject.failed != 0) {
  console.log(downloader.downloadObject.errors)
} else {
    console.log('Download successful !')
}
```
### • Utils function
#### getMusicFolder (sync)
```javascript
deemix.utils.localpaths.getMusicFolder()
```
Return String `'/.../.../Deemix Music/'`
#### getConfigFolder (sync)
```javascript
deemix.utils.localpaths.getConfigFolder()
```
Return String `'/.../.../Deemix Music/'`
#### getAccessToken (async)
```javascript
deemix.utils.deezer.getAccessToken('mail', 'password')
```
**Successful**\
Return String `'access_token'`\
**Error**\
Return `null`
#### getArlFromAccessToken (async)
```javascript
deemix.utils.deezer.getArlFromAccessToken('access_token')
```
**Successful**\
Return String `'arl'`\
**Error**\
Return `null`
## Deezer JS
To communicate with the Deezer API, deemix uses [Deezer-JS](https://gitlab.com/RemixDev/deezer-js).

**Deezer-JS does not have doucmentation !** _we're on it ;)_\
Here is how to use with deemix !

```javascript
const DeezerJS = require('deezer-js') //import DeezerJS
let deezer-js = new DeezerJS.Deezer() //create a new Deezer Instance

async function login() { //new async function for waiting connection...
  var user = await dz.login_via_arl('arl') //login in Deezer
}
login()
```
**You must be logged in to download track !**
## Download Object
```json
{
  "type":"track", //can be track/album/playlist
  "id":"1234567890", //id of track on Deezer
  "bitrate":1, //refer to bitrate below
  "title":"Title", //title
  "artist":"Artist", //artist
  "cover":"https://e-cdns-images.dzcdn.net/images/cover/.../75x75-000000-80-0-0.jpg", // cover URL
  "explicit":false, //explicit
  "size":1, //existing?
  "downloaded":0, //Download state, if 1 download success
  "failed":0, //if 1 download failed
  "progress":0, //Download progress
  "errors":[], //error
  "files":[], //Finded files to download
  "extrasPath":"",
  "progressNext":0, //next number for progress
  "uuid":"track_1234567890_1", //uuid of the track
  "isCanceled":false, //If download is cancelled
  "__type__":"Single", //can be track/album/playlist
  "single": { //object result of DeezerJS search result
    "trackAPI": {
      "id":1234567890, //id of the track on Deezer
      "readable":true, //is music readable ?
      "title":"Title", //title of the track on Deezer
      "title_short":"Very short title", //a short version of the title
      "title_version":"", //???
      "isrc":"ABCD12345678", //isrc of the track
      "link":"https://www.deezer.com/track/...", //link of the track on Deezer
      "share":"https://www.deezer.com/track/...", //share link of the track on Deezer
      "duration":148, //duration in seconds of the track on Deezer
      "track_position":1, //position in album
      "disk_number":1, //disk number
      "rank":1, //rank on Deezer
      "release_date":"2023-02-24", //realease date on Deezer
      "explicit_lyrics":false, //music containing explicit lyrics ?
      "explicit_content_lyrics":0, //???
      "explicit_content_cover":2, //music containing explicit cover ?
      "preview":"https://cdns-preview-c.dzcdn.net/stream/file.mp3", //link of a mp3 file containig 30sec of music for preview
      "bpm":0, //BPM, 0 if not
      "gain":-6.4, //gain
      "available_countries":["AA","BB"], //array of country avaibility
      "contributors": [{ //array of contributors
        "id":1234567, //id of contributors
        "name":"Example", //name of contributors
        "link":"https://www.deezer.com/artist/...", //link of contributors
        "share":"https://www.deezer.com/artist/...", //share link of contributors
        "picture":"https://api.deezer.com/artist/.../image", //picture of contributors
        "picture_small":"https://e-cdns-images.dzcdn.net/images/artist/.../56x56-000000-80-0-0.jpg",
        "picture_medium":"https://e-cdns-images.dzcdn.net/images/artist/.../250x250-000000-80-0-0.jpg",
        "picture_big":"https://e-cdns-images.dzcdn.net/images/artist/.../500x500-000000-80-0-0.jpg",
        "picture_xl":"https://e-cdns-images.dzcdn.net/images/artist/.../1000x1000-000000-80-0-0.jpg",
        "radio":true, //contributors have a radio ?
        "tracklist":"https://api.deezer.com/artist/.../top?limit=50", //radio of contributors
        "type":"artist", //type of contributors
        "role":"Main" //???
      }],
      "md5_image":"f9987df039dfca8e7439f53ce9f470bd", //cover in md5 format
      "artist": {
        "id":5653273,
        "name":"Sound Of Legend",
        "link":"https://www.deezer.com/artist/5653273",
        "share":"https://www.deezer.com/artist/5653273?utm_source=deezer&utm_content=artist-5653273&utm_term=0_1681726055&utm_medium=web",
        "picture":"https://api.deezer.com/artist/5653273/image","picture_small":"https://e-cdns-images.dzcdn.net/images/artist/8e756f21088368aac6bebd68cd21e35b/56x56-000000-80-0-0.jpg",
        "picture_medium":"https://e-cdns-images.dzcdn.net/images/artist/8e756f21088368aac6bebd68cd21e35b/250x250-000000-80-0-0.jpg",
        "picture_big":"https://e-cdns-images.dzcdn.net/images/artist/8e756f21088368aac6bebd68cd21e35b/500x500-000000-80-0-0.jpg",
        "picture_xl":"https://e-cdns-images.dzcdn.net/images/artist/8e756f21088368aac6bebd68cd21e35b/1000x1000-000000-80-0-0.jpg",
        "radio":true,
        "tracklist":"https://api.deezer.com/artist/5653273/top?limit=50",
        "type":"artist"
      },
      "album": {
        "id":404504147,
        "title":"Some Kind Of Kiss",
        "link":"https://www.deezer.com/album/404504147",
        "cover":"https://api.deezer.com/album/404504147/image",
        "cover_small":"https://e-cdns-images.dzcdn.net/images/cover/f9987df039dfca8e7439f53ce9f470bd/56x56-000000-80-0-0.jpg",
        "cover_medium":"https://e-cdns-images.dzcdn.net/images/cover/f9987df039dfca8e7439f53ce9f470bd/250x250-000000-80-0-0.jpg",
        "cover_big":"https://e-cdns-images.dzcdn.net/images/cover/f9987df039dfca8e7439f53ce9f470bd/500x500-000000-80-0-0.jpg",
        "cover_xl":"https://e-cdns-images.dzcdn.net/images/cover/f9987df039dfca8e7439f53ce9f470bd/1000x1000-000000-80-0-0.jpg",
        "md5_image":"f9987df039dfca8e7439f53ce9f470bd",
        "release_date":"2023-02-24",
        "tracklist":"https://api.deezer.com/album/404504147/tracks",
        "type":"album"
      },
      "type":"track"
    }
  }
}
```
## Bitrate
| **Bitrate** | Quality                        |
|:-----------:|--------------------------------|
|    **0**    | MP3_MISC (Can't be downloaded) |
|    **1**    | MP3_128                        |
|    **2**    | MP3_128                        |
|    **3**    | MP3_320                        |
|    **4**    | MP3_320                        |
|    **5**    | MP3_320                        |
|    **6**    | MP3_320                        |
|    **7**    | MP3_320                        |
|    **8**    | MP3_320                        |
|    **9**    | FLAC                           |
